# smoothing using FWHM 6mm gaussian kernel
fslmaths mp.nii.gz -kernel gauss 2.5480 -fmean mp_mp.nii.gz;

# generate rois for the three RSNs and their important regions
fslmaths /usr/local/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz -mul 0 -add 1 -roi 27 1 74 1 40 1 0 1 AI.nii.gz -odt float
fslmaths AI.nii.gz -kernel sphere 6 -fmean AIsphere.nii.gz -odt float
fslmaths /usr/local/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz -mul 0 -add 1 -roi 44 1 69 1 58 1 0 1 ACC.nii.gz -odt float
fslmaths ACC.nii.gz -kernel sphere 6 -fmean ACCsphere.nii.gz -odt float
fslmaths /usr/local/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz -mul 0 -add 1 -roi 28 1 72 1 66 1 0 1 DLPFC.nii.gz -odt float
fslmaths DLPFC.nii.gz -kernel sphere 6 -fmean DLPFCsphere.nii.gz -odt float
fslmaths /usr/local/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz -mul 0 -add 1 -roi 23 1 36 1 65 1 0 1 PPC.nii.gz -odt float
fslmaths PPC.nii.gz -kernel sphere 6 -fmean PPCsphere.nii.gz -odt float
fslmaths /usr/local/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz -mul 0 -add 1 -roi 50 1 32 1 46 1 0 1 PREC.nii.gz -odt float
fslmaths PREC.nii.gz -kernel sphere 6 -fmean PRECsphere.nii.gz -odt float
fslmaths /usr/local/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz -mul 0 -add 1 -roi 43 1 90 1 32 1 0 1 VMPFC.nii.gz -odt float
fslmaths VMPFC.nii.gz -kernel sphere 6 -fmean VMPFCsphere.nii.gz -odt float

# calculate first eigenvariate for each roi
fslmeants -i mp_mp.nii.gz -o AI_eig_mp_mp.txt -m AIsphere.nii.gz --eig --order=1 -v 
fslmeants -i mp_mp.nii.gz -o ACC_eig_mp_mp.txt -m ACCsphere.nii.gz --eig --order=1 -v
fslmeants -i mp_mp.nii.gz -o DLPFC_eig_mp_mp.txt -m DLPFCsphere.nii.gz --eig --order=1 -v
fslmeants -i mp_mp.nii.gz -o PPC_eig_mp_mp.txt -m PPCsphere.nii.gz --eig --order=1 -v 
fslmeants -i mp_mp.nii.gz -o PREC_eig_mp_mp.txt -m PRECsphere.nii.gz --eig --order=1 -v 
fslmeants -i mp_mp.nii.gz -o VMPFC_eig_mp_mp.txt -m VMPFCsphere.nii.gz --eig --order=1 -v
