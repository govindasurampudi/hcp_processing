import os
import sys

pth = sys.argv[1] 

#structural processing
#=====================
os.system("5ttgen fsl "+pth+"/T1w_acpc_dc_restore_brain.nii.gz "+pth+"/5TT.mif -premasked -force")
os.system("5tt2vis "+pth+"/5TT.mif "+pth+"/vis.mif -force")
#os.system("mrview vis.mif")
os.system("labelconvert "+pth+"/aparc+aseg.nii.gz /global/CM/freesurfer/FreeSurferColorLUT.txt /global/mrtrix3/share/mrtrix3/labelconvert/fs_default.txt "+pth+"/nodes.mif -force")
os.system("labelsgmfix "+pth+"/nodes.mif "+pth+"/T1w_acpc_dc_restore_brain.nii.gz /global/mrtrix3/share/mrtrix3/labelconvert/fs_default.txt "+pth+"/nodes_fixSGM.mif -premasked -force")
os.system("rm -rf "+pth+"/nodes.mif")

# remove useless files
os.system("rm -rf "+pth+"/vis.mif")
