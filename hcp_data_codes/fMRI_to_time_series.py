import os
import sys

pth = sys.argv[1]
fMRI = sys.argv[2] # rfMRI_REST2_RL.nii.gz
os.system("print('===========nodes_fixSGM.mif===========')")
os.system("labelconvert "+pth+"/aparc+aseg.nii.gz /global/CM/freesurfer/FreeSurferColorLUT.txt /global/mrtrix3/share/mrtrix3/labelconvert/fs_default.txt "+pth+"/nodes.mif -force")
os.system("labelsgmfix "+pth+"/nodes.mif "+pth+"/T1w_acpc_dc_restore_brain.nii.gz /global/mrtrix3/share/mrtrix3/labelconvert/fs_default.txt "+pth+"/nodes_fixSGM.mif -premasked -force")
os.system("rm -rf "+pth+"/nodes.mif")
os.system('print("========mrconvert===========")')
os.system('time mrconvert '+pth+'/nodes_fixSGM.mif '+pth+'/nodes.nii.gz -force')
os.system('touch '+pth+'/TIME_SERIES.csv')
os.system('print("===time series extraction===")')
os.system('time "$FSL_DIR"/bin/flirt -in '+pth+'/nodes.nii.gz -ref '+pth+'/'+fMRI+' -out '+pth+'/nodes_ntof.nii.gz -omat '+pth+'/ntof.mat -bins 256 -cost corratio -searchrx -90 90 -searchry -90 90 -searchrz -90 90 -dof 12  -interp trilinear')
os.system('time "$FSL_DIR"/bin/fslstats -t -K '+pth+'/nodes_ntof.nii.gz '+pth+'/rfMRI_REST2_RL.nii.gz -M | tee -a '+pth+'/TIME_SERIES.csv')

