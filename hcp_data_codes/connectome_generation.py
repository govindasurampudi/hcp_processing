import os
import sys

pth = sys.argv[1]

#connectome generation
#=====================
os.system("tckgen "+pth+"/WM_FODs.mif "+pth+"/5M.tck -act "+pth+"/5TT.mif -backtrack -crop_at_gmwmi -seed_dynamic "+pth+"/WM_FODs.mif -maxlength 250 -select 10M -cutoff 0.06 -force")
os.system("mrresize "+pth+"/WM_FODs.mif "+pth+"/FOD_downsampled.mif -scale 0.5 -interp sinc -force")
os.system("rm -rf "+pth+"/WM_FODs.mif")
os.system("tcksift "+pth+"/5M.tck "+pth+"/FOD_downsampled.mif "+pth+"/5M_SIFT.tck -act "+pth+"/5TT.mif -term_number 5M -force")
os.system("rm -rf "+pth+"/5M.tck "+pth+"/FOD_downsampled.mif "+pth+"/5TT.mif")
os.system("tck2connectome "+pth+"/5M_SIFT.tck "+pth+"/nodes_fixSGM.mif "+pth+"/connectome.csv -force")
os.system("rm -rf "+pth+"/5M_SIFT.tck")
#os.system("mrview nodes_fixSGM.mif -connectome.init nodes_fixSGM.mif -connectome.load connectome.csv")
