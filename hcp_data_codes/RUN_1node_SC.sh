#!/bin/bash
#SBATCH -A cvit
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -p long
#SBATCH --mem=8g
#SBATCH -t 36:00:00
#SBATCH -o code_log.txt
#SBATCH --mail-type=END
#SBATCH --mail-user=govinda.surampudi@research.iiit.ac.in
#SBATCH --array=0-31 


declare -a subjects=('106521' '106824' '107018' '107220' '107321' '107422' '107725' '108020' '108121' '108222' '108323' '108525' '108828' '109123' '109325' '109830')

bash ./DTI_processing.sh "${subjects[$SLURM_ARRAY_TASK_ID]}"

#time python2.6 RUN.py 
