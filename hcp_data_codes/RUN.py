#!/usr/bin/python

import sys, getopt
import os
import subprocess
import time

def main(argv):
	
	processes = set()
	max_processes = 16 # total number of parallel processes 

	with open('subjectlist.txt', 'r') as f:

    		subjectlist = [line.strip() for line in f]

	for subject in subjectlist:

		cmd = "bash DTI_processing.sh" + " " + str(subject)
		cmd_arg = cmd.split(" ")
		print cmd
    		processes.add(subprocess.Popen(cmd_arg))
    	
	if len(processes) >= max_processes:
        	os.wait()
        	processes.difference_update([p for p in processes if p.poll() is not None])

	for p in processes:
    		if p.poll() is None:
        		p.wait()
    			#os.system(cmd)

if __name__ == "__main__":
   main(sys.argv[1:])
