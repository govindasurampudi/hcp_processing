import os
import sys

pth = sys.argv[1]

#diffusion processing
#====================
os.system("mrconvert "+pth+"/data.nii.gz "+pth+"/DWI.mif -fslgrad "+pth+"/bvecs "+pth+"/bvals -datatype float32 -stride 0,0,0,1 -force")
os.system("dwiextract "+pth+"/DWI.mif - -bzero | mrmath - mean "+pth+"/meanb0.mif -axis 3 -force")
os.system("dwi2response msmt_5tt "+pth+"/DWI.mif "+pth+"/5TT.mif "+pth+"/RF_WM.txt "+pth+"/RF_GM.txt "+pth+"/RF_CSF.txt -voxels "+pth+"/RF_voxels.mif -force")
#os.system("mrview meanb0.mif -overlay.load RF_voxels.mif -overlay.opacity 0.5 -force")
os.system("dwi2fod msmt_csd "+pth+"/DWI.mif "+pth+"/RF_WM.txt "+pth+"/WM_FODs.mif "+pth+"/RF_GM.txt "+pth+"/GM.mif "+pth+"/RF_CSF.txt "+pth+"/CSF.mif -mask "+pth+"/nodif_brain_mask.nii.gz -force")
os.system("rm -rf "+pth+"/DWI.mif "+pth+"/RF_WM.txt "+pth+"/RF_GM.txt "+pth+"/RF_CSF.txt")
os.system("mrconvert "+pth+"/WM_FODs.mif - -coord 3 0 | mrcat "+pth+"/CSF.mif "+pth+"/GM.mif - "+pth+"/tissueRGB.mif -axis 3 -force")
os.system("rm -rf "+pth+"/CSF.mif "+pth+"/GM.mif")
#os.system("mrview tissueRGB.mif -odf.load_sh WM_FODs.mif")

# remove useless files
os.system("rm -rf "+pth+"/meanb0.mif "+pth+"/tissueRGB.mif "+pth+"/RF_voxels.mif")
