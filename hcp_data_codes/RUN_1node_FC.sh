#!/bin/bash
#SBATCH -A cvit
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -p long
#SBATCH --mem=10g
#SBATCH -t 5:00:00
#SBATCH -o code_log_FC.txt
#SBATCH --mail-type=END
#SBATCH --mail-user=govinda.surampudi@research.iiit.ac.in
#SBATCH --array=0-3

declare -a subjects=('107321' '107422' '107725' '108020')

pth="/lustre/cvit/abbhinav/hcp_data/FC_data"
bash fMRI_one_subj.sh "$pth/${subjects[$SLURM_ARRAY_TASK_ID]}"
" 
