AI = dlmread('AI_eig_mp_mp.txt');                                           % right anterior insula
ACC = dlmread('ACC_eig_mp_mp.txt');                                         % anterior cingulate cortex
DLPFC = dlmread('DLPFC_eig_mp_mp.txt');                                     % right dorsolateral prefrontal cortex
PPC = dlmread('PCC_eig_mp_mp.txt');                                         % posterior parietal cortex
PREC = dlmread('PREC_eig_mp_mp.txt');                                       % precuneus
VMPFC = dlmread('VMPFC_eig_mp_mp.txt');                                     % ventromedial prefrontal cortex

TS = [AI,ACC,DLPFC,PPC,PREC,VMPFC];                                         % time-series by concatenating regional time-series
X = dlmread('Movement_Regressors_REST1_LR.txt');                            % movement-regressors for subject brain movement

% TS = X * beta + E 
% here we want to find the residual E
% assume there is no E, then we have TS = X * beta
% solution for beta in the above equation is given by:
%   beta = (X'X)^-1 * X' * TS
% where (X'X)^-1 is left-pseudo inverse of X
% therefore residual is given by:
%   E = TS - X * beta
%   E = TS - X * (X'X)^-1 * X' * TS
E = TS - (X * inv(X'*X) * X' * TS);

% remove first 8 frames to minimize the non-equilibrium effects in fMRI signal.
TS_frm_rm = E(9 : end, :);

% resulting time series were further high-pass  filtered  (f>0.008Hz)
% to remove low-frequency signals related to scanner drift.
L = 1192;
TS_fft = fft(TS_frm_rm, L, 1);
TS_fft_hp([freq,end-freq+1],:) = 0;
TS_frm_rm_hp = ifft(TS_fft_hp, 1192, 1);
TS_frm_rm_hp = real(TS_frm_rm_hp);

% generate FC
FC_frm_rm_hp = corrcoef(TS_frm_rm_hp);
figure,imagesc(FC_frm_rm_hp);