pth="/lustre/cvit/govinda/hcp_data/SC_data/"$1""
echo "==================structural====================="
time python2.6 /lustre/cvit/govinda/hcp_data_codes/structural_processing.py "$pth"
echo "==================diffusion======================"
time python2.6 /lustre/cvit/govinda/hcp_data_codes/diffusion_processing.py "$pth"
echo "==================connectome====================="
time python2.6 /lustre/cvit/govinda/hcp_data_codes/connectome_generation.py "$pth"
#cd /lustre/cvit/govinda/hcp_data_codes/
