#!/bin/bash
#SBATCH -A cvit
#SBATCH -n 1
#SBATCH -N 1 
#SBATCH -p long
#SBATCH --mem=10000
#SBATCH -t 24:00:00
#SBATCH -o code_log.txt
#SBATCH --mail-user=govinda.surampudi@research.iiit.ac.in
##SBATCH --mincpus=7
#SBATCH --cpus-per-task=1

echo "==================structural====================="
time python2.6 /lustre/cvit/govinda/hcp_data_codes/structural_processing.py
echo "==================diffusion======================"
time python2.6 /lustre/cvit/govinda/hcp_data_codes/diffusion_processing.py
echo "==================connectome====================="
time python2.6 /lustre/cvit/govinda/hcp_data_codes/connectome_generation.py

